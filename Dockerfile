FROM node:lts-alpine3.18 AS dev

# Create a working directory docker            #
WORKDIR /usr/src/app

# COPY th package.json package-lock.json ./    #
COPY package*.json ./

#            cleaning at NPM cache             #
RUN npm cache clean --force

#      Install @nestjs/cli dependencies        #
RUN npm install
RUN npm i -g @nestjs/cli

#    copy all files to local docker directory  #
COPY . .

#                 and Build                    #
RUN npm run build --prod


#               Run app                         #
ENTRYPOINT ["node", "dist/main"]

#               Expose port 3000               #
EXPOSE 3000