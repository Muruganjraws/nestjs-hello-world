# Créez une application Nodejs (hello word) à partir d’une image docker Nodejs que vous exposerez sur un port (de votre choix)
## - Phase 1 : Créez une application Nodejs (hello word)
## - Phase 2 : Créez mon Dockerfile NestJS (hello word)
## - Phase 3 : Déployer mon image vers un Pod de mon Cluster

## Activité Type 2 : Déploiement d’une application en continu

#### Dépôt GitLab
`>` https://gitlab.com/commeunatelier/nestjs-hello-world

#### clone repository
`>` https://gitlab.com/commeunatelier/nestjs-hello-world.git

---

# Phase 1 : Créez une application Nodejs (hello word)

## 1) prépare mon environnement NestJS :
" Précision, je travail sur un PC base debian 12 "

### Installation de npm

```
sudo apt update && sudo apt install -y npm
sudo npm install
sudo install @nestjs/cli
```

Je me place dans le répertoire Documents pour travail

```
cd ~/Documents
```

Je crée un nouveau projet nestjs

```
nest new hello-world-nestJS
```

je me déplace dans le nouveau répertoire `hello-world-nest-js`

```
cd hello-world-nest-js/
```
screnshot liste des éléments du répertoire

![screnshot liste des éléments du répertoire](img/affiche-dossier-nest.png)

Je vérifie que je disposes de toutes les dépendances

```
npm install 
```

```
nest @nestjs/cli
```
👍 tout ok on peut passer à l'étape suivante 

screnshot installation dépenses

![screnshot installation dépenses](img/nest-i-nest.png)

## 2) Création de mon app ouverte sur le port 3000

1) je fais une petite personnalisation du fichier `./src/app.service.ts`
😉 _c'est pas utile mais ça me permet de voir que ça fonctionne_

```
import { Injectable } from '@nestjs/common';

@Injectable()
export class AppService {
  getHello(): string {
    return '<center><h1> Hello World !
    </h1> </br>  <h3> Frederic CHAZAL </h3></br>  
    <h2>ECF DevOps
    <br>  
    </br> --------</br> >| App Nodejs |<  </br> --------</h2>
    </center>';
  }
}
```
2) Je lance nestjs

```
nest start
```
screnshot nest start

![screnshot nest start](img/nest-start.png)

Et je peux voir ma page personnalisé 😉

screnshot hello-nest-local

![screnshot hello-nest-local](img/hello-nest-local.png)

# Phase 2 : Créez mon Dockerfile NestJS (hello word)

## 1) installation de docker

J'utilise un de mes petit script d'installation :

```
curl https://gitlab.com/commeunatelier/script_install/-/raw/main/docker_install.sh -o docker_install.sh &&
chmod +x docker_install.sh  && ./docker_install.sh
```

## 2) construction du Dockerfile

Je crée mon fichier dans le répertoire du projet NestJS
```
touch Dockerfile
```
J'utilise un image Nodejs Alpine. Elle présente l'avantage d'être de petite taille
```
FROM node:lts-alpine3.18 AS dev

# Create a working directory docker            #
WORKDIR /usr/src/app

# COPY th package.json package-lock.json ./    #
COPY package*.json ./

#            cleaning at NPM cache             #
RUN npm cache clean --force

#      Install @nestjs/cli dependencies        #
RUN npm install
RUN npm i -g @nestjs/cli

#    copy all files to local docker directory  #
COPY . .

#                 and Build                    #
RUN npm run build --prod


#               Run app                         #
ENTRYPOINT ["node", "dist/main"]

#               Expose port 3000               #
EXPOSE 3000
```

Ensuite j'enregistre mon fichier et je lance un build

```
docker build -t fredcua/nestjs-hello-world:v1.0.0 .
```

screnshot Build Dockerfile

![screnshot Buildl](img/build-docker.png)

Maintenant je vais transférer l'image sur docker Hub. 
a) j'ai créé un compte sur docker Hub
b) j'ai créer un Token qui me permet de connecter mon docker et de pusher vers le repertoire Docker Hub
c) pusher mon image vers docker Hub

```
docker push fredcua/nestjs-hello-world:v1.0.0
```

screnshot docker push

![screnshot push](img/docker-push.png)


**Maintenant mon image est en ligne** 😉


screnshot docker hub

![screnshot hub](img/docker-hub.png)

# Phase 3 : Déployer mon image vers mon Cluster

## 1) Préparer mon Manifest :

### lien vers mon manifest ['deployment-nestjs.yaml`](deployment-nestjs-kube/deployment-nestjs.yaml)

👉 Le `manifest` kube permet de déclarer plusieurs éléments, ici on en a deux l'application et le service de connexion au Load balancer

###🤔 Dans le manifest j'indique :

#### 1er partie app :

 - j'effectue un deployment
 - l'emplacement de mon app (dans le namespace `default` )
 - le nom de mon application `nestjs`
 - le pod s'appelra `srv-nestjs` il sera suivi ensuite par une valeur généré par mon kube
 - le port `3000`
 - les ressources de mon pod

#### 2ème partie service :

 - je déclare un service
 - l'emplacement mon app (dans le namespace `default` )
 - le service s'appel `service-nestjs`
 - il utilise le load balancer public
 - sur le port `3000`

Déploiment de l'application NestJS

```
kubectl create -f deployment-nestjs.yaml
```

screnshot nest-online

![screnshot nest-online](img/nest-online.png)


screnshot view my page 🌞

![screnshot view my page](img/pod-nestjs.png)

fin de la partie 1, 2 et 3 de l'activité 2


## Support

#### Pour mettre en place ce terraform permettant de déploiement de mon cluster, je me suis inspiré des documentations :

**`Tomray.dev :`**
- [ ] _https://www.tomray.dev/nestjs-docker-compose-postgres_

**`docker :`**
- [ ] _https://docs.docker.com/_

## Roadmap
1. Create app nestjs
2. Dockerize app Nestjs
3. deployment kubernetes


## Auteur
**Fred CHAZAL**
